
-- SUMMARY --

User Picture Bulk Upload module help to upload user profile image and avatar.
This module helps to update the user profile image on the basis of UID.
Sometime you need to update the user profile image from any external source,
This will help in mapping the external image url with UID
and update the profile image.

-- REQUIREMENTS --

None.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.

* Its better to add administration menu(admin_menu).

-- CONTACT --

Current maintainers:
* Mahtab Alam - https://www.drupal.org/user/3198311/

This project has been sponsored by:
* SynapseIndia
	SynapseIndia is one of the oldest and among the most trusted Web
	and Mobile Development Companies that has been offering quality
	solutions to global clients since 2001.
